import 'dart:io';
import 'package:http/http.dart'as http;
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:punjab_sind/MainPage.dart';
import 'package:punjab_sind/utils/app_colors.dart';
import 'package:punjab_sind/utils/methods.dart';
import 'package:punjab_sind/utils/raised_button.dart';

class SignUpPage extends StatefulWidget {
 



  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final GlobalKey<FormState>_formkey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
String _email;
String _password;
String _mob_no;
bool loading = false;

 Future<void> _login(context) async{
   String url ="https://api-ecommerce.datavivservers.in/mobile_api/CreateUserRegisterWeb/";
      if(_formkey.currentState.validate()){
        setState(() {
          loading = true;
          
        });
        _formkey.currentState.save();

        try{
          Map data ={
            "username":_email,
            "password":_password,
            "mob_no":_mob_no

          };
          var response = await http.post("$url",body: data);
          var jsonObject = json.decode(response.body);
        
      if(response.statusCode == 200){
            print(jsonObject);
          setState(() {
          loading = false;
        });
      //  Navigator.pushNamed(
      //                         context, "MainPage",
                              
      //                         arguments:{
      //                           "navigationMenu":CustomAppBar,
      //                           "userDetails":jsonObject
                               
      //                         }
      //                       );    


          }else{
           Methods.showSnackBar("Username or password is incorrect", context);
            setState(() {
          loading = false;
        });
            
          }
 


        }on SocketException catch(error){
          setState(() {
          loading = false;
        });
             Methods.showSnackBar(error.toString(), context);
          // print(error);

        }catch(e){
          setState(() {
          loading = false;
        });
            Methods.showSnackBar("Internal Sever Error", context);
          // print(e);
        }
      }

    }
    return Form(
      key:_formkey,
          child: Padding(
        padding: const EdgeInsets.all(28.0),
        child: Center(
          child: Card(
            elevation: 2.0,
            child: Container(
              height: MediaQuery.of(context).size.height / 1.5,
              width: MediaQuery.of(context).size.width / 3,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top:8.0),
                    child: Text("Create Account",style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 18.0, left: 24, right: 24.0),
                    child: TextFormField(
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        hintText: "Mobile Number"
                        
                      ),
                      initialValue: '7305822599',
                      validator: (val) =>
                    val.length < 10 ? 'Enter Valid Mobile Number..' : null,
                onSaved: (val) => _email = val,
                    ),
                  ),
                  Padding(
                   padding:const EdgeInsets.only(top: 18.0, left: 24, right: 24.0),
                    child: TextFormField(
textAlign: TextAlign.center,
obscureText: true,
decoration: InputDecoration(
  hintText: "Password"
),
initialValue: "prabha",
validator: (val) =>
                    val.length < 4 ? 'Password too Short..' : null,
                onSaved: (val) => _password = val,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top:18.0),
                    child: MyRaisedButton(
                      onPressed: _login,
                      loading: loading,
                      title: "SignIn",
                      textColor: AppColors.colorWhite,
                      buttonColor: AppColors.colorRed,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top:18.0),
                    child: MyRaisedButton(
                      onPressed: null,
                      title: "Login With FaceBook",
                      textColor: AppColors.colorWhite,
                      buttonColor: AppColors.colorBlue,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top:18.0,right: 8.0),
                    child: Align(
                      alignment: Alignment.topRight,
                      child: FlatButton(onPressed: null, child: Text("ForgetPassword ?",style: TextStyle(color: AppColors.colorBlue),))),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left:18.0, right:18.0),
                    child: Divider(thickness: 2.0,),
                  ),
                  FlatButton(onPressed: null, child: Text("Create Account",style: TextStyle(color: AppColors.colorBlue),))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
