import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:http/http.dart'as http;
import 'dart:convert';

class CartAmountModel extends Model{

final userToken;

CartAmountModel({
  this.userToken
});

 getCartDetails() async {
      String url =
          "https://api-ecommerce.datavivservers.in/mobile_api/CartDetails/";

      var response = await http.get("$url",
          headers: {"Authorization": "token $userToken"});
      if (response.statusCode == 200) {
        var jsonOject = json.decode(response.body);
        
        return jsonOject;
      } else {
        // Methods.showSnackBar("No data Found", context);
        return null;
      }
    }
}