import 'package:flutter/material.dart';
import 'package:punjab_sind/Screens/CartScreen.dart';

class CartProvider with ChangeNotifier{
  List _cartItem=[];
  List get cartItem=>_cartItem;
  void updateCartItems(data){
    _cartItem.add(data);
    notifyListeners();
  }

  double _cartTotal=0;
  double get cartTotal=>_cartTotal;
  void updateCartTotal(double amount,){
    // _cartTotal=_cartTotal+amount;
    // cartTotal=600+100;
    // cartTotal=600+(100*-1)
    // cart=600-100
   
    _cartTotal+=amount;
    notifyListeners();
  }

  double _cartSubTotal=0;
  double get cartSubTotal=>_cartSubTotal;
  void updateCartSubTotal(double amount){
    _cartSubTotal+=amount;
    notifyListeners();
  }
}