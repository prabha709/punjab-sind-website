import 'package:flutter/material.dart';

class Methods {
  static void showSnackBar(String text, context) {
    SnackBar snackBar = SnackBar(
      margin: EdgeInsets.only(left:320.0,right: 320,bottom: 10.0),
       behavior: SnackBarBehavior.floating,
      
      content: Text(text),);
    Scaffold.of(context)
        .showSnackBar(snackBar);
  }
  static String getEmptyInputError="Cannot be empty";
}