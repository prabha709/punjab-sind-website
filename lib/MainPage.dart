import 'package:flutter/material.dart';
import 'package:punjab_sind/Model/CartModel.dart';
import 'package:punjab_sind/Screens/DairyProducts.dart';
import 'package:punjab_sind/utils/app_colors.dart';
import 'package:punjab_sind/utils/methods.dart';
import 'package:scoped_model/scoped_model.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Map args = ModalRoute.of(context).settings.arguments as Map;

    return Scaffold(
      appBar: CustomAppBar(
        userToken: args["userDetails"],
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              // navigationMenu,
              DairyProducts(
                userDetails: args["userDetails"],
                navigationMenu: args["navigationMenu"],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  final userToken;

  CustomAppBar({this.userToken});
  @override
  get preferredSize => Size.fromHeight(100);

  @override
  Widget build(BuildContext context) {
    int _value = 0;
    CartModel model = CartModel(userDetails: userToken);
    return ScopedModel(
      model: model,
      child: AppBar(
        automaticallyImplyLeading: false,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 18.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Icon(
                  Icons.phone,
                  color: AppColors.colorWhite,
                ),
                SizedBox(
                  width: 5.0,
                ),
                Text("+1-7305822599",
                    style: TextStyle(
                      color: AppColors.colorWhite,
                    )),
                SizedBox(
                  width: 10.0,
                ),
                Icon(
                  Icons.location_on,
                  color: AppColors.colorWhite,
                ),
                SizedBox(
                  width: 5.0,
                ),
                Text("Chennai",
                    style: TextStyle(
                      color: AppColors.colorWhite,
                    )),
                SizedBox(
                  width: 10.0,
                ),
                Text("Login |",
                    style: TextStyle(
                      color: AppColors.colorWhite,
                    )),
                Text("SignUp",
                    style: TextStyle(
                      color: AppColors.colorWhite,
                    ))
              ],
            ),
          ),
        ],
        bottom: PreferredSize(
            child: Container(
              height: MediaQuery.of(context).size.height / 8,
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 18.0),
                    child: Image.asset("logo.png"),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 18.0),
                    child: Text("Home"),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 18.0),
                    child: Text("Shop"),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 18.0),
                    child: InkWell(
                      onTap: (){
                        Navigator.pushNamed(context, "about");
                      },
                      child: Text("About Us")),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 18.0),
                    child: Text("Contact"),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 18.0),
                    child: DropdownButton(
                      underline: SizedBox(),
                      iconSize: 0.0,
                      elevation: 0,
                      value: _value,
                      items: [
                        DropdownMenuItem(
                          child: Text("More"),
                          value: 0,
                        ),
                        DropdownMenuItem(
                          child: InkWell(
                            onTap: () {
                              if (userToken != null) {
                                Navigator.pushNamed(context, "subscription",
                                    arguments: {"userToken": userToken});
                              } else {
                                Methods.showSnackBar(
                                    "Kindly Login Check Subscriptions",
                                    context);
                              }
                            },
                            child: Row(
                              children: [
                                Icon(Icons.notifications),
                                Text("My Subscriptions"),
                              ],
                            ),
                          ),
                          value: 1,
                        ),
                        DropdownMenuItem(
                          child: Row(
                            children: [
                              Icon(Icons.card_giftcard),
                              Text("Orders"),
                            ],
                          ),
                          value: 2,
                        ),
                        DropdownMenuItem(
                          child: Row(
                            children: [
                              Icon(Icons.person),
                              Text("Profile"),
                            ],
                          ),
                          value: 3,
                        ),
                        DropdownMenuItem(
                          child: Row(
                            children: [
                              Icon(Icons.account_balance_wallet),
                              Text("Wallet"),
                            ],
                          ),
                          value: 4,
                        ),
                        DropdownMenuItem(
                          child: Row(
                            children: [
                              Icon(Icons.favorite),
                              Text("Wishlist"),
                            ],
                          ),
                          value: 5,
                        ),
                        DropdownMenuItem(
                          child: Row(
                            children: [
                              Icon(Icons.power),
                              Text("Logout"),
                            ],
                          ),
                          value: 6,
                        ),
                      ],
                      onChanged: (value) {
                        _value = value;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 18.0),
                    child: Icon(Icons.search),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 0),
                    child: Icon(Icons.favorite_border_outlined),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 38.0),
                    child: ScopedModelDescendant<CartModel>(
                      builder: (BuildContext context, Widget child,
                          CartModel cartModel) {
                        return FutureBuilder(
                            future: cartModel.getCount(),
                            builder: (context, snapshot) {
                              if (snapshot.data == null) {
                                return Padding(
                                  padding: const EdgeInsets.all(15.0),
                                  child: Image.asset(
                                    "cart.png",
                                    width: 25.0,
                                  ),
                                );
                              } else {
                                return Stack(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: new IconButton(
                                        onPressed: () {
                                          Navigator.pushNamed(context, "cart",
                                              arguments: {
                                                "userDetails": userToken
                                              });
                                        },
                                        icon: Image.asset(
                                          "cart.png",
                                          width: 25.0,
                                        ),
                                      ),
                                    ),
                                    new Icon(Icons.brightness_1,
                                        size: 21.0, color: Colors.red[800]),
                                    new Positioned(
                                        top: 3.0,
                                        right: 35,
                                        child: new Center(
                                          child: new Text(
                                            snapshot.data["get_cart_items"].toString()
                                                ,
                                            style: new TextStyle(
                                                color: Colors.white,
                                                fontSize: 11.0,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        )),
                                  ],
                                );
                              }
                            });
                      },
                    ),
                  )
                ],
              ),
            ),
            preferredSize: Size.fromHeight(50.0)),
      ),
    );
  }
}
