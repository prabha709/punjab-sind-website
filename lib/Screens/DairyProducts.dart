import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:punjab_sind/Screens/CheeseProducts.dart';
import 'package:punjab_sind/Screens/CreamProducts.dart';
import 'package:punjab_sind/Screens/ManufactureProcess.dart';
import 'package:punjab_sind/Screens/MilkProducts.dart';
import 'package:punjab_sind/Screens/SliderImages.dart';
import 'package:punjab_sind/utils/app_colors.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class DairyProducts extends StatelessWidget {
  final navigationMenu;
  final userDetails;

  DairyProducts({this.navigationMenu,this.userDetails});

  Future getProducts() async {
    // const url ="https://fab-agri-api.herokuapp.com/HomePageContentAPI/";
    String url =
        "https://api-ecommerce.datavivservers.in/mobile_api/HomePageContentAPIWeb/";
    try {
      var response =
          await http.get("$url", headers: {"Accept": "application/json"});
      var jsonObject = json.decode(response.body);
      if (response.statusCode == 200) {
        
        return jsonObject;
      } else {
        print(response.statusCode);
        return null;
      }
    } catch (e) {
      print(e);
    }
  }

  _shopNow(context) {}

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    final double itemWidth = size.width / 2;
    final buttonFlat = FlatButton(
      onPressed: () {},
      child: Text("partners"),
      color: Colors.grey,
    );
    final bottomBar = Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(28.0),
          child: Container(
            height: MediaQuery.of(context).size.height / 2,
            //  color: Colors.pink,
            child: Center(
              child: Text("What is Lorem Ipsum Lorem Ipsum ?"),
            ),
          ),
        ),
        Padding(
            padding: EdgeInsets.all(28.0),
            child: Row(
              children: [
                Text("Our Partners",
                    style:
                        TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold)),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: buttonFlat,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: buttonFlat,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: buttonFlat,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: buttonFlat,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: buttonFlat,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: buttonFlat,
                ),
              ],
            )),
        Padding(
            padding: EdgeInsets.only(top: 10.0),
            child: Container(
              height: MediaQuery.of(context).size.height / 5,
              child: Row(
                children: [
                  Container(
                    color: Colors.blueGrey,
                    width: MediaQuery.of(context).size.width / 4.1,
                  ),
                  Container(
                    color: Colors.brown,
                    width: MediaQuery.of(context).size.width / 4.1,
                  ),
                  Container(
                    color: Colors.cyan,
                    width: MediaQuery.of(context).size.width / 4.1,
                  ),
                  Container(
                    color: Colors.deepPurple,
                    width: MediaQuery.of(context).size.width / 4.1,
                  ),
                ],
              ),
            )),
        Container(
          color: Colors.lightGreen,
          height: MediaQuery.of(context).size.height / 2,
        )
      ],
    );

    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: FutureBuilder(
          future: getProducts(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.data == null) {
                return Center(
                  child: Text("NO Products Found"),
                );
              } else {
                return SingleChildScrollView(
                  child: Column(
                    children: [
                      SliderImages(
                        productDetails: snapshot.data["features"],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top:18.0),
                        child: ManufactureProcess(),
                      ),
                      //Other Dairy Products
                      CreamProducts(
                        userDetails: userDetails,
                        productDetails: snapshot.data,
                        shopNow: _shopNow,
                        navigationMenu: navigationMenu,
                        bottomBar: bottomBar,
                      ),
                      //Cheese|Butter
                      CheeseProducts(
                        userDetails: userDetails,
                        productsDetails: snapshot.data,
                        shopNow: _shopNow,
                        navigationMenu: navigationMenu,
                        bottomBar: bottomBar,
                      ),

                      MilkProducts(
                        userDetails: userDetails,
                        productDetails: snapshot.data,
                        shopNow: _shopNow,
                        navigationMenu: navigationMenu,
                        bottomBar: bottomBar,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 18.0, left: 28.0),
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Text("Our Categories",
                                style: TextStyle(
                                    fontSize: 25.0,
                                    fontWeight: FontWeight.bold))),
                      ),
                      Padding(
                        padding:
                            EdgeInsets.only(top: 18.0, left: 18.0, right: 18.0),
                        child: GridView.builder(
                            shrinkWrap: true,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 3,
                                    crossAxisSpacing: itemWidth / itemHeight),
                            itemCount: snapshot.data["product_category"].length,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.all(18.0),
                                child: Container(
                                  color: Colors.greenAccent,
                                  child: Center(
                                      child: Text(
                                          snapshot.data["product_category"][index]
                                              ["category_name"])),
                                ),
                              );
                            }),
                      ),
                      bottomBar
                    ],
                  ),
                );
              }
            } else if (snapshot.hasError) {
              return Center(
                child: Text("Internal Server Error"),
              );
            } else {
              return Center(
                child: SpinKitThreeBounce(color: AppColors.colorBlue),
              );
            }
          },
        ),
      
    );
  }
}

// Container(
//         height: MediaQuery.of(context).size.height,
//         width: MediaQuery.of(context).size.width,
//         child: FutureBuilder(
//           future: getProducts(),
//           builder: (context, snapshot) {
//             if (snapshot.connectionState == ConnectionState.done) {
//               if (snapshot.data == null) {
//                 return Center(
//                   child: Text("NO Products Found"),
//                 );
//               } else {
//                 return SingleChildScrollView(
//                   child: Column(
//                     children: [
//                       SliderImages(),
//                       ManufactureProcess(),
//                       //Other Dairy Products
//                       CreamProducts(
//                         userDetails: userDetails,
//                         productDetails: snapshot.data,
//                         shopNow: _shopNow,
//                         navigationMenu: navigationMenu,
//                         bottomBar: bottomBar,
//                       ),
//                       //Cheese|Butter
//                       CheeseProducts(
//                         productsDetails: snapshot.data,
//                         shopNow: _shopNow,
//                         navigationMenu: navigationMenu,
//                         bottomBar: bottomBar,
//                       ),

//                       MilkProducts(
//                         productDetails: snapshot.data,
//                         shopNow: _shopNow,
//                         navigationMenu: navigationMenu,
//                         bottomBar: bottomBar,
//                       ),
//                       Padding(
//                         padding: EdgeInsets.only(top: 18.0, left: 28.0),
//                         child: Align(
//                             alignment: Alignment.topLeft,
//                             child: Text("Our Categories",
//                                 style: TextStyle(
//                                     fontSize: 25.0,
//                                     fontWeight: FontWeight.bold))),
//                       ),
//                       Padding(
//                         padding:
//                             EdgeInsets.only(top: 18.0, left: 18.0, right: 18.0),
//                         child: GridView.builder(
//                             shrinkWrap: true,
//                             gridDelegate:
//                                 SliverGridDelegateWithFixedCrossAxisCount(
//                                     crossAxisCount: 3,
//                                     crossAxisSpacing: itemWidth / itemHeight),
//                             itemCount: snapshot.data["product_category"].length,
//                             itemBuilder: (context, index) {
//                               return Padding(
//                                 padding: const EdgeInsets.all(18.0),
//                                 child: Container(
//                                   color: Colors.greenAccent,
//                                   child: Center(
//                                       child: Text(
//                                           snapshot.data["product_category"][index]
//                                               ["category_name"])),
//                                 ),
//                               );
//                             }),
//                       ),
//                       bottomBar
//                     ],
//                   ),
//                 );
//               }
//             } else if (snapshot.hasError) {
//               return Center(
//                 child: Text("Internal Server Error"),
//               );
//             } else {
//               return Center(
//                 child: SpinKitThreeBounce(color: AppColors.colorBlue),
//               );
//             }
//           },
//         ),
      
//     );