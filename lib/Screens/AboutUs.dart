import 'package:flutter/material.dart';
import 'package:punjab_sind/MainPage.dart';

class AboutUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: Container(
        child: SingleChildScrollView(
                  child: Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height/1.3,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("shop.png"),
                    fit: BoxFit.fill,
                  )
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Text("About Us"),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text("What is Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry's standard dummyWhat is Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry's standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has?"),
              )
            ],
          ),
        ),
      ),
      
    );
  }
}