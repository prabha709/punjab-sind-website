import 'package:flutter/material.dart';
import 'package:punjab_sind/MainPage.dart';

class AddDeliveryAddress extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:SingleChildScrollView(
              child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top:8.0),
              child: Center(child: Text("Delivery Address",style: TextStyle(fontSize: 25.0,fontWeight: FontWeight.bold),)),
            ),
            Padding(
              padding: const EdgeInsets.all(28.0),
              child: Container(
                
                width: MediaQuery.of(context).size.width,
                color: Color(0xffF0F0F0),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left:8.0,top:8.0),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Text("Personal Details")),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(28.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Enter First Name"),
                              Container(
                            width: 200,
                            child: TextFormField(
                              
                              decoration: InputDecoration(
                                border: new OutlineInputBorder(),
                              ),
                            ),
                          ),

                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Enter Last Name"),
                              Container(
                            width: 200,
                            child: TextFormField(
                              
                              decoration: InputDecoration(
                                border: new OutlineInputBorder(),
                              ),
                            ),
                          ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Mobile Number"),
                              Container(
                            width: 200,
                            child: TextFormField(
                              
                              decoration: InputDecoration(
                                border: new OutlineInputBorder(),
                              ),
                            ),
                          ),
                            ],
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left:8.0,top:8.0),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Text("Address Details")),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(28.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("* House/Flat No."),
                              Container(
                            width: 200,
                            child: TextFormField(
                              
                              decoration: InputDecoration(
                                border: new OutlineInputBorder(),
                              ),
                            ),
                          ),

                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Enter Last Name"),
                              Container(
                            width: 600,
                            child: TextFormField(
                              
                              decoration: InputDecoration(
                                border: new OutlineInputBorder(),
                              ),
                            ),
                          ),
                            ],
                          ),
                          
                        ],
                      ),
                    ),
                     Padding(
                      padding: const EdgeInsets.all(28.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("* House/Flat No."),
                              Container(
                            width: 500,
                            child: TextFormField(
                              
                              decoration: InputDecoration(
                                border: new OutlineInputBorder(),
                              ),
                            ),
                          ),

                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Enter Last Name"),
                              Container(
                            width: 400,
                            child: TextFormField(
                              
                              decoration: InputDecoration(
                                border: new OutlineInputBorder(),
                              ),
                            ),
                          ),
                            ],
                          ),
                          
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(28.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Enter First Name"),
                              Container(
                            width: 200,
                            child: TextFormField(
                              
                              decoration: InputDecoration(
                                border: new OutlineInputBorder(),
                              ),
                            ),
                          ),

                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Enter Last Name"),
                              Container(
                            width: 200,
                            child: TextFormField(
                              
                              decoration: InputDecoration(
                                border: new OutlineInputBorder(),
                              ),
                            ),
                          ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Mobile Number"),
                              Container(
                            width: 200,
                            child: TextFormField(
                              
                              decoration: InputDecoration(
                                border: new OutlineInputBorder(),
                              ),
                            ),
                          ),
                            ],
                          )
                        ],
                      ),
                    ),
                    
                     Padding(
                      padding: const EdgeInsets.only(left:8.0,top:8.0),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Text("Address Type")),
                    ),
                    Row(
                      children: [
                        RaisedButton(
                          child: Text("Home"),
                          onPressed: (){},
                        ),
                        RaisedButton(
                          child: Text("Office"),
                          onPressed: (){},
                        ),
                        
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right:28.0),
                      child: Align(
                        alignment: Alignment.topRight,
                        child: RaisedButton(
                          color: Colors.blue,
                            child: Text("Add Address"),
                            onPressed: (){},
                          ),
                      ),
                    )

                  ],
                ),
              ),
            ),
            
          ],
        ),
      ),
      // appBar: CustomAppBar(),
      
    );
  }
}