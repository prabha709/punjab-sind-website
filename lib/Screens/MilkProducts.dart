import 'package:flutter/material.dart';
import 'package:punjab_sind/utils/app_colors.dart';
import 'package:punjab_sind/utils/methods.dart';
import 'package:punjab_sind/utils/raised_button.dart';
import 'package:http/http.dart'as http;
import 'dart:convert';

class MilkProducts extends StatelessWidget {
  final productDetails;
  final shopNow;
  final navigationMenu;
  final bottomBar;
  final userDetails;

  MilkProducts({
    this.userDetails,
    this.bottomBar,
    this.productDetails, this.shopNow, this.navigationMenu});

  final ScrollController _controller = ScrollController();
  @override
  Widget build(BuildContext context) {
    

    _addToSubScribe(indexData){
   Navigator.pushNamed(context, "createsubscription", arguments:{
     "productDetails":indexData
   });
    }
    Future _addToCart(productId) async {  
      String url =
          "https://api-ecommerce.datavivservers.in/mobile_api/AddToCart/";

      Map data = {"product_id": productId, "user_id": userDetails["userId"]};

       var encodedData = json.encode(data);
       print(encodedData);
      print(userDetails["token"]);
      try {
        var response = await http.post(
          "$url",
          headers: {
            "Authorization": "token ${userDetails["token"]}",
            "Accept": "application/json",
            "Content-type": "application/json",
          },
          body: json.encode(data)
        );
        var jsonObject = jsonDecode(response.body);
        if (response.statusCode == 200) {
          Methods.showSnackBar("Product Added Successfuly", context);
          // return jsonObject;
        } else {
          print(response.body);
          Methods.showSnackBar("Product Not yet Added", context);
          return null;
        }
      } catch (e) {
        print(e);
      }
    }
    final itemSize = MediaQuery.of(context).size.width / 4.8;
    _moveNext() {
      _controller.animateTo(_controller.offset + itemSize * 3,
          duration: Duration(milliseconds: 500), curve: Curves.linear);
    }

    _movePrevious() {
      _controller.animateTo(_controller.offset - itemSize * 3,
          duration: Duration(milliseconds: 500), curve: Curves.linear);
    }

    final rightArrow = Positioned(
        right: 0,
        top: 50,
        bottom: 50,
        child: InkWell(
            onTap: () {
              _moveNext();
            },
            child: CircleAvatar(
                backgroundColor: Colors.grey[200],
                child: Center(child: Icon(Icons.arrow_right)))));
    final leftArrow = Positioned(
        left: 0,
        top: 50,
        bottom: 50,
        child: InkWell(
            onTap: () {
              _movePrevious();
            },
            child: CircleAvatar(
                backgroundColor: Colors.grey[200],
                child: Center(child: Icon(Icons.arrow_left)))));
    return Padding(
      padding: const EdgeInsets.only(top: 18.0),
      child: Column(
        children: [
          Align(
              alignment: Alignment.topLeft,
              child: Padding(
                  padding: EdgeInsets.only(top: 10.0, left: 28.0),
                  child: Row(
                    children: [
                      Text("Most Popular".toUpperCase(),
                          style: TextStyle(fontSize: 30.0)),
                      SizedBox(
                        width: 10.0,
                      ),
                      Text(
                        "Mild| Lassi",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.italic,
                            fontSize: 30.0),
                      ),
                    ],
                  ))),
          Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: const EdgeInsets.only(left: 35.0, top: 10.0),
              child: Card(
                elevation: 2.0,
                child: Container(
                  width: MediaQuery.of(context).size.width / 2,
                  height: MediaQuery.of(context).size.height / 15,
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 18.0, top: 8.0),
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Icon(Icons.menu)),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 18.0),
                        child: Text("All".toUpperCase()),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 18.0),
                        child: Text("Farm sour Cream".toUpperCase()),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width / 6,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 18.0),
                        child: InkWell(
                          onTap: () {
                            Navigator.pushNamed(
                              context, "productList",
                              
                              arguments:{
                                "navigationMenu": navigationMenu,
                                  "productDetails":productDetails["most_popular_milk_lassi"],
                                  "categoryDetails":productDetails["product_category"],
                                  "bottomBar":bottomBar,
                                  "userToken":userDetails
                              }
                            );
                          },
                          child: Text(
                            "View All",
                            style: TextStyle(color: AppColors.colorBlue),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Container(
                        width: MediaQuery.of(context).size.width / 1.6,
                        height: MediaQuery.of(context).size.height / 1.8,
                        child: Stack(
                          children: [
                            ListView.builder(
                              controller: _controller,
                              scrollDirection: Axis.horizontal,
                              shrinkWrap: true,
                              itemCount:
                                  productDetails["most_popular_milk_lassi"]
                                      .length,
                              itemBuilder: (context, index) {
                                return Padding(
                                  padding: const EdgeInsets.all(18.0),
                                  child: Card(
                                    elevation: 2.0,
                                    child: Container(
                                      width:
                                          MediaQuery.of(context).size.width / 6,
                                      child:
                                       Center(
                                          child: Column(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.all(18.0),
                                            child: Container(
                                                height: MediaQuery.of(context).size.height /5,
                                                decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                    image: NetworkImage("https://api-ecommerce.datavivservers.in"+productDetails["most_popular_milk_lassi"][index]["product_images_URL"]),
                                                    fit:BoxFit.fitHeight
                                                  )
                                                ),
                                            ),
                                          ),
                                               Text(productDetails["most_popular_milk_lassi"][index]["product_name"],
                                          style: TextStyle(fontWeight: FontWeight.bold,fontSize: 10.0),),
                                          Text("Rs:"+ productDetails["most_popular_milk_lassi"][index]["product_price"].toString(),
                                          style:TextStyle(color: Color(0xff83B9B2))),
                                          Padding(
                                            padding: const EdgeInsets.only(left:8.0,right: 8.0),
                                            child: Divider(
                                              thickness: 1.0,
                                            ),
                                          ),
                                          FlatButton(
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Icon(Icons
                                                    .shopping_bag_outlined),
                                                Text("Add to Cart"),
                                              ],
                                            ),
                                            onPressed: () {
                                              _addToCart(
                                                  productDetails["most_popular_milk_lassi"][index]["product_id"]);
                                            },
                                          ),
                                          productDetails["most_popular_milk_lassi"][index]["subscription"] == true ?FlatButton(
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Icon(Icons.notifications),
                                                Text("Subscribe"),
                                              ],
                                            ),
                                            onPressed: () {
                                              _addToSubScribe(
                                                  productDetails["most_popular_milk_lassi"][index]);
                                            },
                                          ):Container()

                                        ],
                                      ),
                                        // child: Text(productDetails["most_popular_milk_lassi"][index]["product_name"]),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                            rightArrow,
                            leftArrow
                          ],
                        ),
                      ))),
              Padding(
                padding: const EdgeInsets.only(right: 28.0),
                child: Image.network("https://api-ecommerce.datavivservers.in"+productDetails["most_popular_milk_lassi_img"][0]["img"],
                width: MediaQuery.of(context).size.width/6
                ),
              ),
            ],
          ),
          // Align(
          //   alignment: Alignment.topLeft,
          //   child: Padding(
          //     padding: EdgeInsets.only(top: 10.0, left: 35.0),
          //     child: MyRaisedButton(
          //       title: "Shop Now",
          //       buttonColor: AppColors.colorYellow,
          //       onPressed: shopNow,
          //       textColor: Colors.black,
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }
}
