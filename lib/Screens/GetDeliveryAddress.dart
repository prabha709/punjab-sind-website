import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:punjab_sind/MainPage.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:punjab_sind/utils/methods.dart';

String _deliveryType = "Pick Up";
String paymentType = "Cod";

class GetDeliveryAddress extends StatefulWidget {
  @override
  _GetDeliveryAddressState createState() => _GetDeliveryAddressState();
}

class _GetDeliveryAddressState extends State<GetDeliveryAddress> {
  @override
  Widget build(BuildContext context) {
    Map args = ModalRoute.of(context).settings.arguments as Map;

    return Scaffold(
        appBar: CustomAppBar(),
        body: Column(
          children: [
            Container(),
            GetAddressData(args: args),
            Card(
              elevation: 2.0,
              child: Column(
                children: [
                  Text(
                    "DeliveryType",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  DeliveryType(),
                  Divider(),
                  Text(
                    "DeliveryType",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Column(
                    children: [
                      ListTile(
                        title: Text("Cash on Deliver"),
                        leading: Radio(
                          value: "Cod",
                          groupValue: paymentType,
                          onChanged: (value) {
                            setState(() {
                              paymentType = value;
                            });
                            print(paymentType);
                          },
                        ),
                      ),
                      ListTile(
                        title: Text("Online Payment"),
                        leading: Radio(
                          value: "Online Payment",
                          groupValue: paymentType,
                          onChanged: (value) {
                            setState(() {
                              paymentType = value;
                            });
                            print(paymentType);
                          },
                        ),
                      ),
                    ],
                  ),
                  RaisedButton(
                    onPressed: (){

                    },
                    color: Theme.of(context).primaryColor,
                    child: paymentType == "Cod"?Text("Proceed"):Text("Make Payment"),
                  )
                ],
              ),
            ),
          ],
        ));
  }
}

class GetAddressData extends StatefulWidget {
  final args;
  const GetAddressData({
    this.args,
    Key key,
  }) : super(key: key);

  @override
  _GetAddressDataState createState() => _GetAddressDataState();
}

class _GetAddressDataState extends State<GetAddressData> {
  getDeliveryDetails() async {
    String url =
        "https://api-ecommerce.datavivservers.in/mobile_api/ShippingAddressWeb/";

    var response = await http.get("$url", headers: {
      "Authorization": "token ${widget.args["userToken"]["token"]}",
    });
    if (response.statusCode == 200) {
      var jsonOject = json.decode(response.body);

      return jsonOject[0];
    } else {
      Methods.showSnackBar("No data Found", context);
      return null;
    }
  }

  Future test;
  @override
  void initState() {
    super.initState();
    test = getDeliveryDetails();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: test,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.data == null) {
            return Container(
              padding: EdgeInsets.only(left: 18.0, right: 18.0),
              width: MediaQuery.of(context).size.width / 3,
              height: MediaQuery.of(context).size.height / 6,
              decoration: BoxDecoration(border: Border.all()),
              child: Center(
                child: Text("+ \t Add Address"),
              ),
            );
          } else {
            return Padding(
              padding: const EdgeInsets.all(18.0),
              child: Card(
                elevation: 2.0,
                child: Container(
                  padding: EdgeInsets.only(left: 18.0, right: 18.0),
                  width: MediaQuery.of(context).size.width / 3,
                  // height: MediaQuery.of(context).size.height/4,

                  child: Column(
                    children: [
                      Row(
                        children: [
                          Icon(Icons.location_on),
                          Text("DeliveryAddress"),
                          SizedBox(
                            width: 50.0,
                          ),
                          FlatButton(
                            color: Colors.blue,
                            shape: RoundedRectangleBorder(),
                            onPressed: () {},
                            child: Text("change"),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Text(snapshot.data["address1"].toString()),
                          Text(snapshot.data["address2"]),
                          Text(snapshot.data["pincode"].toString())
                        ],
                      ),
                      Divider(),
                    ],
                  ),
                ),
              ),
            );
          }
        } else if (snapshot.hasError) {
          return Center(
            child: Text("Internal Server Error"),
          );
        } else {
          return SpinKitThreeBounce(
            color: Theme.of(context).primaryColor,
          );
        }
      },
    );
  }
}

class DeliveryType extends StatefulWidget {
  const DeliveryType({
    Key key,
  }) : super(key: key);

  @override
  _DeliveryTypeState createState() => _DeliveryTypeState();
}

class _DeliveryTypeState extends State<DeliveryType> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          title: Text("Delivery"),
          leading: Radio(
            value: "Delivery",
            groupValue: _deliveryType,
            onChanged: (value) {
              setState(() {
                _deliveryType = value;
              });
            },
          ),
        ),
        ListTile(
          title: Text("Pick Up"),
          leading: Radio(
            value: "Pick Up",
            groupValue: _deliveryType,
            onChanged: (value) {
              setState(() {
                _deliveryType = value;
              });
            },
          ),
        ),
      ],
    );
  }
}
