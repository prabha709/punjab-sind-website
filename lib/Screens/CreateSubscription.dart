import 'package:flutter/material.dart';
import 'package:punjab_sind/MainPage.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';


class CreateSubScription extends StatefulWidget {
  @override
  _CreateSubScriptionState createState() => _CreateSubScriptionState();
}

class _CreateSubScriptionState extends State<CreateSubScription> {
  String _value = "Daily";
  int _itemCount = 1;
  String startDate;
  String endDate;
  @override
  Widget build(BuildContext context) {
    Map args = ModalRoute.of(context).settings.arguments as Map;
    _addSubscription() async {
      String url =
          "https://api-ecommerce.datavivservers.in/mobile_api/ProductSubscribeWeb/";
      Map data = {
        "product_id": args["productDetails"]["product_id"],
        "user_id": args["userDetails"]["userId"],
        "schedule_type":_value,
        "subscription_start_date":startDate,
        "subscription_end_date":endDate,
      };
      try {
        var response =
            await http.post("$url", 
            body: json.encode(data),
            headers: {"Accept": "application/json",
            "Content-Type":"application/json",
            "Authorization":"token ${args["userDetails"]["token"]}"
            });
        var jsonObject = json.decode(response.body);
        if (response.statusCode == 200) {
          print(response.body);
          return jsonObject;
        } else {
          print(response.body);
          // Methods.showSnackBar(response.body, context);
          return null;
        }
      } catch (e) {
        print(e);
      }
    }

    return Scaffold(
        appBar: CustomAppBar(),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Create Subscription",
                  style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.underline),
                ),
              ),
            ),
            Container(
                padding: const EdgeInsets.all(18.0),
                height: MediaQuery.of(context).size.height / 2.5,
                width: MediaQuery.of(context).size.width / 1.6,
                child: Card(
                  elevation: 2.0,
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(18.0),
                        child: Image.network(
                            "https://api-ecommerce.datavivservers.in" +
                                args["productDetails"]["product_images_URL"]),
                      ),
                      Column(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 18.0),
                            child: Text(args["productDetails"]["product_name"]),
                          ),
                          Text("Rs." +
                              args["productDetails"]["product_price"]
                                  .toString()),
                          // Text("Quantity"),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Row(
                              children: [
                                new IconButton(
                                    icon: new Icon(Icons.add),
                                    onPressed: () =>
                                        setState(() => _itemCount++)),
                                SizedBox(
                                  width: 5.0,
                                ),
                                Text(_itemCount.toString()),
                                SizedBox(
                                  width: 5.0,
                                ),
                                _itemCount != 0
                                    ? new IconButton(
                                        icon: new Icon(Icons.remove),
                                        onPressed: () =>
                                            setState(() => _itemCount--),
                                      )
                                    : new Container(),
                              ],
                            ),
                          ),
                          DropdownButton(
                              value: _value,
                              items: [
                                DropdownMenuItem(
                                  child: Text("Daily"),
                                  value: "Daily",
                                ),
                                DropdownMenuItem(
                                  child: Text("Alternate Days"),
                                  value: "Alternate Days",
                                ),
                                DropdownMenuItem(
                                  
                                    child: Text("Every 3 Days"), value: "Every 3 Days"),
                                DropdownMenuItem(
                                    child: Text("Weekly"), value: "Weekly")
                              ],
                              onChanged: (value) {
                                setState(() {
                                  _value = value;
                                });
                              }),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top:28.0),
                        child: Column(
                          children: [
                            
                            startDate !=null ?Text(startDate):Text("Select Start date"),
                            RaisedButton(
                              onPressed: () {
                                DatePicker.showDatePicker(
                                  context,
                                  showTitleActions: true,
                                  minTime: DateTime(2019, 1, 1),
                                  maxTime: DateTime(2023, 1, 1),
                                  onChanged: (date) {
                                    // print('change $date');
                                  },
                                  onConfirm: (date) {
                                    var dates = DateTime.parse(date.toString());
        var formattedDate = "${dates.year}-${dates.month}-${dates.day}";
        
                                    setState(() {
                                      startDate = formattedDate.toString();
                                    });
                                    // print('confirm $date');
                                  },
                                );
                              },
                              child: Text("Start Date"),
                            ),
                            endDate !=null ?Text(endDate):Text("Select End date"),
                            RaisedButton(
                              onPressed: () {
                                DatePicker.showDatePicker(
                                  context,
                                  showTitleActions: true,
                                  minTime: DateTime(2019, 1, 1),
                                  maxTime: DateTime(2023, 1, 1),
                                  onChanged: (date) {
                                    // print('change $date');
                                  },
                                  onConfirm: (date) {
                                    print(date);
                                     var dates = DateTime.parse(date.toString());
        var formattedDate = "${dates.year}-${dates.month}-${dates.day}";
        
                                    setState(() {
                                      endDate = formattedDate.toString();
                                    });
                                    // print('confirm $date');
                                  },
                                );
                              },
                              child: Text("End Date"),
                            )
                          ],
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomRight,
                                              child: Padding(
                                                padding: const EdgeInsets.only(bottom:8.0,),
                                                child: RaisedButton(
                          color: Colors.yellow,
                          onPressed: () {
                            _addSubscription();
                          },
                          child: Text("Subscribe",style: TextStyle(fontWeight: FontWeight.bold)),
                        ),
                                              ),
                      ),

                    ],
                  ),
                )),
          ],
        ));
  }
}
