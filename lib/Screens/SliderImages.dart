import 'package:flutter/material.dart';

import 'package:flutter_swiper/flutter_swiper.dart';

class SliderImages extends StatelessWidget {
 
final productDetails;

SliderImages({
  this.productDetails
});
  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.green,
      height: MediaQuery.of(context).size.height / 1.8,
      child: Swiper(
        itemCount: 3,
        autoplay: true,
        autoplayDelay: 5000,
        itemBuilder: (context, index) {
          return new Image.network("https://api-ecommerce.datavivservers.in"+productDetails[index]["img"]);
        },
      ),
    );
    //   return  Container(
    //       height:MediaQuery.of(context).size.height/1.5,
    //       child: FutureBuilder(
    //         future: getProducts(),
    //         builder: (context, snapshot){
    //           if(snapshot.connectionState == ConnectionState.done){
    //             if(snapshot.data == null){
    //               return Center(
    //                 child: Text("No Products"),
    //               );
    //             }else{
    //               return ListView.builder(
    //                 itemCount: snapshot.data["trending_products"].length,
    //                 itemBuilder: (context, index){
    //                   return Card(
    //                     child: Text(snapshot.data["trending_products"][index]["product_name"]),
    //                   );
    //                 },

    //               );
    //             }
    //           }else if(snapshot.hasError){
    //   return Center(
    //     child: Text("Internal Server Error"),

    //   );
    // }else{
    //   return Center(
    //     child: SpinKitThreeBounce(color: AppColors.colorBlue),

    //   );
    // }
    //         },
    //       ),
    //     );
  }
}
