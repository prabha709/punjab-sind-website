import 'package:flutter/material.dart';
import 'package:punjab_sind/utils/app_colors.dart';
import 'package:punjab_sind/utils/methods.dart';

import 'package:http/http.dart'as http;
import 'dart:convert';

class CheeseProducts extends StatelessWidget {
  final productsDetails;
  final userDetails;
 final navigationMenu;
 final bottomBar;
  final shopNow;
  
  CheeseProducts({
    this.userDetails,
    this.bottomBar,
    this.navigationMenu,
    this.productsDetails,this.shopNow});
  final ScrollController _controller = ScrollController();
  @override
  Widget build(BuildContext context) {
      _addToSubScribe(indexData){
   Navigator.pushNamed(context, "createsubscription", arguments:{
     "productDetails":indexData,
     "userDetails":userDetails
   });
    }
     Future _addToCart(productId) async {
      String url =
          "https://api-ecommerce.datavivservers.in/mobile_api/AddToCart/";

      Map data = {"product_id": productId, "user_id": userDetails["userId"]};

       var encodedData = json.encode(data);
       print(encodedData);
      print(userDetails["token"]);
      try {
        var response = await http.post(
          "$url",
          headers: {
            "Authorization": "token ${userDetails["token"]}",
            "Accept": "application/json",
            "Content-type": "application/json",
          },
          body: json.encode(data)
        );
        var jsonObject = jsonDecode(response.body);
        if (response.statusCode == 200) {
          print(response.body);
          Methods.showSnackBar("Product Added Successfuly", context);
          // return jsonObject;
        } else {
          print(response.body);
          Methods.showSnackBar("Product Not yet Added", context);
          return null;
        }
      } catch (e) {
        print(e);
      }
    }
    final itemSize = MediaQuery.of(context).size.width / 4.8;
    return Padding(
      padding: const EdgeInsets.only(top: 18.0),
      child: Column(
        children: [
          Align(
              alignment: Alignment.topLeft,
              child: Padding(
                  padding: EdgeInsets.only(top: 10.0, right: 170.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text("THE Finest".toUpperCase(),
                          style: TextStyle(fontSize: 30.0)),
                      SizedBox(
                        width: 10.0,
                      ),
                      Text(
                        "Cheese| Butter",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.italic,
                            fontSize: 30.0),
                      ),
                    ],
                  ))),
                  Row(
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width/2.5,
                      ),
                      Align(
      alignment: Alignment.topLeft,
      child: Padding(
        padding: const EdgeInsets.only(left: 35.0, top: 10.0),
        child: Card(
          elevation: 2.0,
          child: Container(
            width: MediaQuery.of(context).size.width / 2,
            height: MediaQuery.of(context).size.height / 15,
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 18.0, top: 8.0),
                  child: Align(
                      alignment: Alignment.topLeft, child: Icon(Icons.menu)),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 18.0),
                  child: Text("All".toUpperCase()),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 18.0),
                  child: Text("Farm sour Cream".toUpperCase()),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width / 6,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 18.0),
                  child: InkWell(
                    onTap: () {
                            Navigator.pushNamed(
                              context, "productList",
                              
                              arguments:{
                                "navigationMenu": navigationMenu,
                                  "productDetails":productsDetails["finest_cheese_butter"],
                                  "categoryDetails":productsDetails["product_category"],
                                  "bottomBar":bottomBar,
                                  "userToken":userDetails
                              }
                            );
                          },
                                      child: Text(
                      "View All",
                      style: TextStyle(color: AppColors.colorBlue),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    )

                    ],
                  ),
                  
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.network("https://api-ecommerce.datavivservers.in"+productsDetails["finest_cheese_butter_img"][0]["img"],
              width:MediaQuery.of(context).size.width/6 
              ,),
              Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                      padding: const EdgeInsets.only(right: 20.0),
                      child: Container(
                        width: MediaQuery.of(context).size.width / 1.6,
                        height: MediaQuery.of(context).size.height / 1.8,
                        child: Stack(
                          children: [
                            ListView.builder(
                              controller: _controller,
                              scrollDirection: Axis.horizontal,
                              shrinkWrap: true,
                              itemCount: productsDetails["finest_cheese_butter"].length,
                              itemBuilder: (context, index) {
                                return Padding(
                                  padding: const EdgeInsets.all(18.0),
                                  child: Card(
                                    elevation: 2.0,
                                    child: Container(
                                      width:MediaQuery.of(context).size.width / 6,
                                      child: Center(
                                        child: Column(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.all(18.0),
                                            child: Container(
                                                height: MediaQuery.of(context).size.height /5,
                                                decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                    image: NetworkImage("https://api-ecommerce.datavivservers.in"+productsDetails["finest_cheese_butter"][index]["product_images_URL"] ),
                                                    fit:BoxFit.fitHeight
                                                  )
                                                ),
                                            ),
                                          ),
                                               Text(productsDetails["finest_cheese_butter"][index]["product_name"],
                                          style: TextStyle(fontWeight: FontWeight.bold,fontSize: 10.0),),
                                          Text("Rs:"+ productsDetails["finest_cheese_butter"][index]["product_price"].toString(),
                                          style:TextStyle(color: Color(0xff83B9B2))),
                                          Padding(
                                            padding: const EdgeInsets.only(left:8.0,right: 8.0),
                                            child: Divider(
                                              thickness: 1.0,
                                            ),
                                          ),
                                          FlatButton(
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Icon(Icons
                                                    .shopping_bag_outlined),
                                                Text("Add to Cart"),
                                              ],
                                            ),
                                            onPressed: () {
                                              _addToCart(
                                                  productsDetails["finest_cheese_butter"][index]["product_id"]);
                                            },
                                          ),
                                          productsDetails["finest_cheese_butter"][index]["subscription"] == true ?FlatButton(
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Icon(Icons.notifications),
                                                Text("Subscribe"),
                                              ],
                                            ),
                                            onPressed: () {
                                              _addToSubScribe(
                                                  productsDetails["finest_cheese_butter"][index]);
                                            },
                                          ):Container()
                                        ],
                                      ),

                                        // child: Text(
                                        //     productsDetails["finest_cheese_butter"][index]["product_name"]),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                            
                            Positioned(
                                right: 0,
                                top: 50,
                                bottom: 50,
                                child: InkWell(
                                    onTap: () {
                                      _controller.animateTo(
                                          _controller.offset + itemSize * 3,
                                          duration: Duration(milliseconds: 500),
                                          curve: Curves.linear);
                                    },
                                    child: CircleAvatar(
                                        backgroundColor: Colors.grey[200],
                                        child: Center(
                                            child: Icon(Icons.arrow_right))))),
                            Positioned(
                                left: 0,
                                top: 50,
                                bottom: 50,
                                child: InkWell(
                                    onTap: () {
                                      _controller.animateTo(
                                          _controller.offset - itemSize * 3,
                                          duration: Duration(milliseconds: 500),
                                          curve: Curves.linear);
                                    },
                                    child: CircleAvatar(
                                        backgroundColor: Colors.grey[200],
                                        child: Center(
                                            child: Icon(Icons.arrow_left))))),
                          ],
                        ),
                      ))),
            ],
          ),
          // Align(
          //   alignment: Alignment.topRight,
          //   child: Padding(
          //     padding: EdgeInsets.only(top: 10.0, right: 35.0),
          //     child: MyRaisedButton(
          //       title: "Shop Now",
          //       buttonColor: AppColors.colorYellow,
          //       onPressed: shopNow,
          //       textColor: Colors.black,
          //     ),
          //   ),
          // ),

        ],
      ),
    );
  }
}
