import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:punjab_sind/MainPage.dart';
import 'package:http/http.dart' as http;
import 'package:punjab_sind/Model/cart.dart';
import 'dart:convert';
import 'package:punjab_sind/utils/methods.dart';

double cartTotal = 0;
double cartSubTotal = 0;
double deliveryCharge = 0;

class CartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    CartProvider cartProvider=Provider.of<CartProvider>(context);
    Map args = ModalRoute.of(context).settings.arguments as Map;
    getCartDetails() async {
      String url =
          "https://api-ecommerce.datavivservers.in/mobile_api/CartDetails/";

      var response = await http.get("$url",
          headers: {"Authorization": "token ${args["userDetails"]["token"]}"});
      if (response.statusCode == 200) {
        var jsonOject = json.decode(response.body);

        return jsonOject;
      } else {
        Methods.showSnackBar("No data Found", context);
        return null;
      }
    }

    return Scaffold(
      appBar: CustomAppBar(),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Text(
              "Cart".toUpperCase(),
              style: TextStyle(fontSize: 30.0),
            ),
          ),
          FutureBuilder(
            future: getCartDetails(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.data == null) {
                  return Center(
                    child: Text("Cart is Empty"),
                  );
                } else {
                  return Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(18.0),
                        child: Card(
                          elevation: 2.0,
                          child: Container(
                            height: MediaQuery.of(context).size.height / 2,
                            width: MediaQuery.of(context).size.width / 2.2,
                            child: ListView.separated(
                              itemCount:cartProvider.cartItem.length,
                              // itemCount: snapshot.data["products"].length,
                              separatorBuilder: (_, index) {
                                return Padding(
                                  padding: const EdgeInsets.only(
                                      left: 8.0, right: 8.0),
                                  child: Divider(thickness: 2),
                                );
                              },
                              itemBuilder: (context, index) {
                                return ProductCard(
                                  // productDetail: snapshot.data["products"]
                                  //     [index],
                                  // token: args['userDetails'],
                                  productDetail: cartProvider.cartItem[index]['data'],
                                  token: cartProvider.cartItem[index]['token']
                                );
                              },
                            ),
                          ),
                        ),
                      ),
                      Card(
                        elevation: 2.0,
                                              child: Container(
                          
                              height: MediaQuery.of(context).size.height / 2,
                              width: MediaQuery.of(context).size.width / 2.2,
                          child: CartAmountDetails(
                            productAmount: snapshot.data,
                          ),
                        ),
                      )
                    ],
                  );
                }
              } else if (snapshot.hasError) {
                return Center(
                  child: Text("Internal Server Error"),
                );
              } else {
                return Center(
                    child: SpinKitThreeBounce(
                  color: Theme.of(context).primaryColor,
                ));
              }
            },
          ),
        ],
      ),
    );
  }
}

class CartAmountDetails extends StatefulWidget {
  final productAmount;
  const CartAmountDetails({this.productAmount});

  @override
  _CartAmountDetailsState createState() => _CartAmountDetailsState();
}

class _CartAmountDetailsState extends State<CartAmountDetails> {
  @override
  @override
  void initState() {
    super.initState();
    cartTotal = widget.productAmount["get_cart_total"];
    cartSubTotal = widget.productAmount["get_cart_sub_total"];
    deliveryCharge = widget.productAmount["deliveryCharges"];
    Future.delayed(
        Duration.zero,
        () => {
              Provider.of<CartProvider>(context, listen: false)
                  .updateCartTotal(widget.productAmount["get_cart_total"]),
              Provider.of<CartProvider>(context, listen: false)
                  .updateCartSubTotal(
                      widget.productAmount["get_cart_sub_total"]),
            });
  }

  @override
  Widget build(BuildContext context) {
    CartProvider cartProvider = Provider.of<CartProvider>(context);

    return Column(
      children: [
        Padding(
          padding: EdgeInsets.all(10.0),
          child: Text(
            "Cart Total",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25.0),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 18.0, right: 18.0, top: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("subTotal"),
              Text("Rs." + cartProvider.cartSubTotal.toString())
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(
            left: 18.0,
            right: 18.0,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Delivery Charges"),
              Text("Rs." + deliveryCharge.toString())
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(
            left: 18.0,
            right: 18.0,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Grand Total"),
              Text("Rs." + cartProvider.cartTotal.toString())
            ],
          ),
        ),
        Align(
          alignment:Alignment.bottomRight ,
          child: Padding(
            padding: const EdgeInsets.only(top:8.0,right: 18.0),
            child: RaisedButton(
              onPressed: (){},
              color: Theme.of(context).primaryColor,
              child: Text("Check Out",style: TextStyle(fontWeight: FontWeight.bold)),
            ),
          )
          ,)
      ],
    );
  }
}

class ProductCard extends StatefulWidget {
  const ProductCard({
    @required this.token,
    @required this.productDetail,
  });
  final token;
  final productDetail;

  @override
  _ProductCardState createState() => _ProductCardState();
}

class _ProductCardState extends State<ProductCard> {
  int productPrice;
  int quantity;

  @override
  void initState() {
    super.initState();
    productPrice = widget.productDetail['get_product_total'];
    quantity = widget.productDetail["product_quantity"];
  }

  _updateData(String operations, indexData) async {
    String url =
        "https://api-ecommerce.datavivservers.in/mobile_api/CartQuantityActions/";
    try {
      Map data = {
        "user_id": widget.token["userId"],
        "product_id": indexData,
        "action": operations
      };

      var response = await http.post("$url", body: json.encode(data), headers: {
        "Accept": "application/json",
        "Content-type": "application/json",
        "Authorization": "token ${widget.token['token']}"
      });
      print(response.body);
      var jsonObject = json.decode(response.body);
      if (response.statusCode == 200) {
        Methods.showSnackBar("Updation Done", context);

        setState(() {
          operations == "decrement" ? quantity -= 1 : quantity += 1;
          operations == "decrement"
              ? productPrice -= widget.productDetail["product_price"]
              : productPrice += widget.productDetail["product_price"];
        });
      } else {
        return Methods.showSnackBar("Updation Not Done", context);
      }
      return jsonObject;
    } on SocketException catch (error) {
      Methods.showSnackBar(error.toString(), context);
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    CartProvider cartProvider = Provider.of<CartProvider>(context);
    return ListTile(
      title: Text(widget.productDetail["product_name"]),
      subtitle: Row(
        children: [
          Row(
            children: [
              InkWell(
                  onTap: () async {
                    print(widget.token);
                    cartProvider
                        .updateCartTotal(widget.productDetail["product_price"]);
                    cartProvider.updateCartSubTotal(
                        widget.productDetail["product_price"]);
                    _updateData(
                        "increment", widget.productDetail["product_id"]);
                  },
                  child: Icon(Icons.add)),
              Text(quantity.toString()),
              InkWell(
                  onTap: () async {
                    print(widget.token);
                    cartProvider.updateCartTotal(
                        widget.productDetail["product_price"] * -1);
                    cartProvider.updateCartSubTotal(
                        widget.productDetail["product_price"] * -1);
                    _updateData(
                        "decrement", widget.productDetail["product_id"]);
                  },
                  child: Icon(Icons.remove)),
            ],
          ),
          Text("Rs." + productPrice.toString())
        ],
      ),
      leading: Image.network(
          "https://api-ecommerce.datavivservers.in/images/productImages/PS_Desi_Ghee_Jar.png"),
      // leading: Image.network("https://api-ecommerce.datavivservers.in${snapshot.data["products"][index]["product_images"]}"),
    );
  }
}
