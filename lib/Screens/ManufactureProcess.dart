import 'package:flutter/material.dart';
import 'package:punjab_sind/utils/app_colors.dart';


class ManufactureProcess extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height/3,
      
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
         children: <Widget>[
           Padding(
             padding: const EdgeInsets.only(left: 38.0),
             child: Text("What We Do\nThe finesh Products\nfrom our Dairy", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 28.0),)
           ),
           Row(
             children: [
               Column(
             mainAxisAlignment: MainAxisAlignment.center,
             children: [
               Padding(
                 padding: const EdgeInsets.only(right:28.0),
                 child: CircleAvatar(
                    radius: 70,
                    backgroundColor:AppColors.colorGrey,
                    child: CircleAvatar(
                      backgroundColor: Color(0xff3586FF),
                      radius: 66,
                      child: Image.asset("cow.png",width: 60.0,),
                    ),
                  ),
               ),
              
               Padding(
                 padding: const EdgeInsets.only(right:18.0,top: 10.0),
                 child: Text("Nature\nMilk for Making Ghee",textAlign: TextAlign.center,),
               )
             ],
           ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right:28.0),
                    child: CircleAvatar(
                    radius: 70,
                    backgroundColor: AppColors.colorGrey,
                    child: CircleAvatar(
                      backgroundColor: AppColors.colorWhite,
                      radius: 65,
                      child:Image.asset("home.png",width: 60.0,),
                    ),
            ),
                  ),
                  Padding(
                 padding: const EdgeInsets.only(right:18.0,top: 10.0),
                 child: Text("Farm\nTechnology Leader",textAlign: TextAlign.center,),
               )
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right:38.0),
                    child: CircleAvatar(
                    radius: 70,
                    backgroundColor: AppColors.colorGrey,
                    child: CircleAvatar(
                      backgroundColor: AppColors.colorWhite,
                      radius: 65,
                       child: Image.asset("tractor.png",width: 60.0,),
                    ),
            ),
                  ),
                  Padding(
                 padding: const EdgeInsets.only(right:18.0,top: 10.0),
                 child: Text("convenient\nDelivery System",textAlign: TextAlign.center,),
               )
                ],
              )

             ],
           )
           

         ],
      ),
      
    );
  }
}