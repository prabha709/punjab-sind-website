import 'package:flutter/material.dart';
import 'package:punjab_sind/MainPage.dart';


class ProductList extends StatelessWidget {
  
  
  final ScrollController _controller = ScrollController();
  @override
  Widget build(BuildContext context) {
    Map  args = ModalRoute.of(context).settings.arguments as Map;
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 23) / 0.8;
    final double itemWidth = size.width / 2;
    final itemSize = MediaQuery.of(context).size.width / 5.5;
    _moveNext() {
      _controller.animateTo(_controller.offset + itemSize * 1,
          duration: Duration(milliseconds: 500), curve: Curves.linear);
    }

    _movePrevious() {
      _controller.animateTo(_controller.offset - itemSize * 1,
          duration: Duration(milliseconds: 500), curve: Curves.linear);
    }

    final rightArrow = Positioned(
        right: 0,
        top: 50,
        bottom: 50,
        child: InkWell(
            onTap: () {
              _moveNext();
            },
            child: CircleAvatar(
                backgroundColor: Colors.grey[200],
                child: Center(child: Icon(Icons.arrow_right)))));
    final leftArrow = Positioned(
        left: 0,
        top: 50,
        bottom: 50,
        child: InkWell(
            onTap: () {
              _movePrevious();
            },
            child: CircleAvatar(
                backgroundColor: Colors.grey[200],
                child: Center(child: Icon(Icons.arrow_left)))));
    return Scaffold(
      appBar: CustomAppBar(userToken: args['userToken']),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Align(alignment: Alignment.topLeft, child: Text("home > Lassi ")),
              Stack(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height / 3,
                    child: ListView.builder(
                        controller: _controller,
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: args['categoryDetails'].length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              width: MediaQuery.of(context).size.width / 5,
                              color: Colors.blueGrey,
                              child: Center(
                                  child: Text(
                                      args["categoryDetails"][index]["category_name"])),
                            ),
                          );
                        }),
                  ),
                  leftArrow,
                  rightArrow
                ],
              ),
              Container(
                child: Padding(
                  padding: const EdgeInsets.all(28.0),
                  child: GridView.builder(
                    shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 4,
                          childAspectRatio: itemWidth / itemHeight),
                      itemCount: args['productDetails'].length,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {},
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(
                                  color: Color(0xffc4c4c4),
                                )),
                            padding: EdgeInsets.all(8),
                            margin: EdgeInsets.all(8),
                            child: Column(
                              children: [
                                //Product Image
                                Container(
                                  height: MediaQuery.of(context).size.height / 4,
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Image(
                                      fit: BoxFit.cover,
                                      image: NetworkImage("https://www.reversefactor.in/blogs/73getty-854296650.jpg"),
                                    ),
                                  ),
                                ),

                                //Product name
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    args["productDetails"][index]["product_name"],
                                    style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 13,
                                    ),
                                  ),
                                ),

                                //Product Price
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    //orignal price
                                    Text(
                                      '₹ 0',
                                      style: TextStyle(
                                          decoration: TextDecoration.lineThrough),
                                    ),
                                    SizedBox(width: 8),
                                    //offer price
                                    Text(
                                      '₹' + args["productDetails"][index]["product_price"].toString(),
                                      style: TextStyle(
                                        // decoration: TextDecoration.lineThrough,
                                        color: Theme.of(context).primaryColor,
                                      ),
                                    )
                                  ],
                                ),

                                //add To Cart button
                                FlatButton(
                                  onPressed: () {},
                                  splashColor: Colors.redAccent,
                                  color: Theme.of(context).accentColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: Text(
                                    'ADD TO CART',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                               args["productDetails"][index]["subscription"] == true ? Text(
                                 'Subscribe',
                                 style: TextStyle(color: Theme.of(context).primaryColor,fontWeight: FontWeight.bold),
                               ): Container()
                              ],
                            ),
                          ),
                        );
                      }),
                ),
              ),
              args["bottomBar"]
            ],
          ),
        ),
      ),
    );
  }
}
