import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:punjab_sind/Login/Loginpage.dart';
import 'package:punjab_sind/MainPage.dart';
import 'package:punjab_sind/Model/cart.dart';
import 'package:punjab_sind/MoreOptions/Subscriptions.dart';
import 'package:punjab_sind/Screens/AboutUs.dart';
import 'package:punjab_sind/Screens/CartScreen.dart';
import 'package:punjab_sind/Screens/CreateSubscription.dart';
import 'package:punjab_sind/Screens/DeliveryAddress.dart';
import 'package:punjab_sind/Screens/GetDeliveryAddress.dart';
import 'package:punjab_sind/Screens/ProductList.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => CartProvider(),
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Punjab Sind Dairy Products',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        initialRoute: 'home',
        routes: {
          'home': (context) => MyHomePage(),
          'productList': (context) => ProductList(),
          "login": (context) => LoginPage(),
          "MainPage": (context) => MainPage(),
          "cart": (context) => CartScreen(),
          "subscription":(context)=> Subscriptions(),
          "addDeliveryAddress":(context)=>AddDeliveryAddress(),
          "about":(context)=> AboutUs(),
          "createsubscription":(context)=>CreateSubScription(),
          "getDeliveryAddress":(context)=>GetDeliveryAddress() 
        },
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),

      // body:CreateSubScription()
      body: LoginPage(),
      //  MainPage(navigationMenu: navigationMenu,),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
